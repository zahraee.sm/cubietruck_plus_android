#ifndef DEINTERLACE_SW_H
#define DEINTERLACE_SW_H

#include <deinterlace.h>
#include <sw-deinterlace/AWPostProcess.h>

class DeinterlaceSw : public Deinterlace
{
private:
	CAWPostProcess *awPP;
    
public:
    DeinterlaceSw();

    ~DeinterlaceSw();

    int init();
    
    int reset();

    EPIXELFORMAT expectPixelFormat();

    int flag();
    
    int process(VideoPicture *pPrePicture,
                VideoPicture *pCurPicture,
                VideoPicture *pOutPicture,
                int nField);

};

#endif
