#include <stdio.h>
#include "swapi.h"
#include "swtype.h"
#include "swlog.h"
#include "swmutex.h"
#include "swthrd.h"
#include "swplayerdef.h"
#include "swavstage.h"
#include "CTC_MediaProcessorImpl.h"
 
using namespace android;

#define plog_debug(...) sw_log(LOG_LEVEL_DEBUG, "STAGE", (char *)__FUNCTION__, __LINE__, __VA_ARGS__)
#define plog_info(...)  sw_log(LOG_LEVEL_INFO, "STAGE", (char *)__FUNCTION__, __LINE__, __VA_ARGS__)
#define plog_warn(...)  sw_log(LOG_LEVEL_WARN, "STAGE", (char *)__FUNCTION__, __LINE__, __VA_ARGS__)
#define plog_error(...) sw_log(LOG_LEVEL_ERROR, "STAGE", (char *)__FUNCTION__, __LINE__, __VA_ARGS__)
#define plog_fatal(...) sw_log(LOG_LEVEL_FATAL, "STAGE", (char *)__FUNCTION__, __LINE__, __VA_ARGS__)

static CTC_MediaProcessor *m_player=NULL;

/**
 * swavstage_t
 **/
struct _swavstage
{
	int started;
	int status;
	swpigmode_e mode;
	
	swrect_t rect;

	long last_pts;
	int pts_same_count;

	void *event_handle;
	avs_event_callback p_event_callback;

	media_stream_t *p_media;
};

/**
 * swcodecmap_t
 **/
typedef struct _swcodecmap_t
{
	swcodec_type_t codec_id;
	int format;
}swcodecmap_t;

/**
 * video codec��Ӧ��
 **/
swcodecmap_t g_swvcodecmap[] =
{
	{ VIDEO_AVS,        VFORMAT_AVS },
	{ VIDEO_H264,       VFORMAT_H264 },
	{ VIDEO_MJPEG,      VFORMAT_MJPEG },                                                                                         
	{ VIDEO_MP2V,       VFORMAT_MPEG12 },
	{ VIDEO_MP4V,       VFORMAT_MPEG4 },
	{ VIDEO_RV40,       VFORMAT_REAL },
	{ VIDEO_VC1,        VFORMAT_VC1},
};

/**
 * audio codec��Ӧ��
 **/
swcodecmap_t g_swacodecmap[] =
{
	{ AUDIO_MPEG,       FORMAT_MPEG },
	{ AUDIO_PCM_S16LE,  FORMAT_PCM_S16LE }, 
	{ AUDIO_AAC,        FORMAT_AAC },
	{ AUDIO_AC3,        FORMAT_AC3 },
	{ AUDIO_DTS,        FORMAT_DTS },
	{ AUDIO_FLAC,       FORMAT_FLAC },
	{ AUDIO_COOK,       FORMAT_COOK },
	{ AUDIO_ADPCM,      FORMAT_ADPCM },
	{ AUDIO_WMA1,       FORMAT_WMA },
	{ AUDIO_WMAPRO,     FORMAT_WMAPRO },
	{ AUDIO_PCM_BLURAY, FORMAT_PCM_BLURAY },
	{ AUDIO_ALAC,       FORMAT_ALAC },
	{ AUDIO_VORBIS,     FORMAT_VORBIS },
};

static void MediaProcessorEvent(IPTV_PLAYER_EVT_e evt, void *handler, unsigned long param1, unsigned long param2);

/**
 * @brief codecת�� 
 * @param codec_id codec���� 
 * @return 0:�ɹ�����ֵ:ʧ��
 */
int swcodec2vformat(swcodec_type_t codec_id)
{
	int i;
	for (i = 0; i < (int)(sizeof(g_swvcodecmap) / sizeof(g_swvcodecmap[0])); i++)
	{
		if (g_swvcodecmap[i].codec_id == codec_id)
			return g_swvcodecmap[i].format;
	}
	return 0;
}

/**
 * @brief codecת��
 * @param codec_id codec���� 
 * @return 0:�ɹ�����ֵ:ʧ��
 */
int swcodec2aformat(swcodec_type_t codec_id)
{
	int i;
	for (i = 0; i < (int)(sizeof(g_swacodecmap) / sizeof(g_swacodecmap[0])); i++)
	{
		if (g_swacodecmap[i].codec_id == codec_id)
			return g_swacodecmap[i].format;
	}
	return 0;
}

/**
 * @brief bitpersampleת��
 * @param codec_id codec���� 
 * @return bit_per_samle ������ 
 */
int swformat2bitpersample(swaudio_format_e sample_format)
{
	int bit_per_sample = 0;
	switch(sample_format)
	{
		case SW_AUDIOFMT_U8:
		case SW_AUDIOFMT_S8:
			 bit_per_sample = 8;
			 break;
		case SW_AUDIOFMT_U16LE:
		case SW_AUDIOFMT_U16BE:
		case SW_AUDIOFMT_S16LE:
		case SW_AUDIOFMT_S16BE:
			 bit_per_sample = 16;
			 break;
		case SW_AUDIOFMT_S24LE:
		case SW_AUDIOFMT_S24BE:
			 bit_per_sample = 24;
			 break;
		case SW_AUDIOFMT_S32LE:
		case SW_AUDIOFMT_S32BE:
			 bit_per_sample = 32;
			 break;
		default:
			 bit_per_sample = 8;
			 break;
	}
	return bit_per_sample;
}

void MediaProcessorEvent(IPTV_PLAYER_EVT_e evt, void *handler, unsigned long param1, unsigned long param2)
{
	swavstage_t *avsobj = (swavstage_t *)handler;
	switch (evt)
	{
		case IPTV_PLAYER_EVT_FIRST_PTS:
			avsobj->p_event_callback(avsobj->event_handle, AVS_EVENT_VIDEOFIRSTPTS, 0, 0, 0);
			break;
		case IPTV_PLAYER_EVT_ABEND://TODO
			break;
		default:
			break;
	}
}

/**
 * @brief avstage��ʼ��
 * @return 0:�ɹ�����ֵ:ʧ��
 */
int sw_avstage_init()
{
	GetMediaProcessorVersion();

	m_player = GetMediaProcessor();
	return 0;
}

/**
 * @brief avstage���
 * @return ��
 */
void sw_avstage_release()
{
	delete m_player;
}

/** 
 * @brief ��avstage
 * @param cap ���蹦������
 * @return ��� NULL:ʧ��
 */
swavstage_t* sw_avstage_open(const mediacap_t *cap)
{
	swavstage_t *avsobj;
	avsobj = (swavstage_t *)malloc(sizeof(swavstage_t));
	if (avsobj == NULL)
	{
		plog_error("malloc FAILED!\n");
		return NULL;
	}

	memset(avsobj, 0, sizeof(swavstage_t));
	avsobj->mode = SW_PIGMODE_FULL;

	return avsobj;
}

/** 
 * @brief �ر�avstage
 * @param avsobj avstage������
 * @return ��
 */
void sw_avstage_close(swavstage_t *avsobj)
{
	if (avsobj != NULL)
		free(avsobj);
	
	return ;
}

/** 
 * @brief ��ȡ�ڲ���Դ
 * @param avsobj avstage������
 * @return 0:�ɹ�����ֵ:ʧ��
 */
int sw_avstage_inner_init(swavstage_t *avsobj)
{
	return 0;
}

/** 
 * @brief �ڲ��ͷ���Դ
 * @param avsobj avstage������
 * @return ��
 */
void sw_avstage_inner_release(swavstage_t *avsobj)
{
	return ;
}

/* @brief sw_avstageset_caps����player������, ���������ܵ�ǰδʹ�� 
 * @param caps��e_mediacap����ı���λ��, ��Ϊ0��ʾ�Ȳ�������Դ
 * @return �ɹ�����0, ʧ�ܷ���-1
 */
int sw_avstage_set_caps(swavstage_t *avsobj, const mediacap_t *cap)
{
	if(avsobj == NULL || m_player == NULL)
		return 0;

	return 0;
}

/** 
 * @brief sw_avstageget_caps��ȡplayer������, ���������ܵ�ǰδʹ�� 
 * @param caps�������, �ɹ�ʱ����������䵽�˲�����
 * @return �ɹ�����0, ʧ�ܷ���-1
 */
int sw_avstage_get_caps(swavstage_t *avsobj, mediacap_t *cap/*out*/)
{
	if(avsobj == NULL || m_player == NULL)
		return 0;
	
	return 0;
}

/**
 * @brief 			����avstage
 * @param avsobj 	avstage ������ 
 * @param media  	��Ҫչʾ��ý������Ϣ
 * @param startmode ����ģʽ
 * @return 0 �ɹ� 	���� �����
 */
int sw_avstage_start(swavstage_t* avsobj, media_stream_t* media, e_avs_startmode startmode, uint32_t wparam)
{
	plog_error("Func:%s In\n", __FUNCTION__);
	VIDEO_PARA_T VideoPara;
	if(media->system_num == 0)
		VideoPara.pid = 0xffff;
	else
		VideoPara.pid = media->videos[media->video_index].pid;
	VideoPara.nVideoWidth = media->videos[media->video_index].frame_width;
	VideoPara.nVideoHeight = media->videos[media->video_index].frame_height;
	VideoPara.vFmt = (vformat_t)swcodec2vformat(media->videos[media->video_index].codec);
	plog_info("video pid = %d\n",VideoPara.pid);
	plog_info("video codec = %d\n",media->videos[media->video_index].codec);
	plog_info("VideoPara.nVideoWidth = %d VideoPara.nVideoHeight = %d\n", VideoPara.nVideoWidth, VideoPara.nVideoHeight);
	m_player->InitVideo(&VideoPara);
	
	AUDIO_PARA_T AudioPara;
	if(media->system_num == 0)
		AudioPara.pid = 0xffff;
	else
		AudioPara.pid = media->audios[media->audio_index].pid;
	AudioPara.nSampleRate = media->audios[media->audio_index].samplerate;
	AudioPara.nChannels = media->audios[media->audio_index].channels;
	AudioPara.block_align = media->audios[media->audio_index].block_align;
	AudioPara.bit_per_sample = swformat2bitpersample(media->audios[media->audio_index].sample_format);
	AudioPara.nExtraSize = media->audios[media->audio_index].spec_len;
	if(AudioPara.nExtraSize)
	{
		AudioPara.pExtraData = (unsigned char *)malloc(AudioPara.nExtraSize * sizeof(unsigned char));
		if(AudioPara.pExtraData)
			memcpy(AudioPara.pExtraData, media->audios[media->audio_index].spec_data, AudioPara.nExtraSize);
	}
	AudioPara.aFmt = (aformat_t)swcodec2aformat(media->audios[media->audio_index].codec);
	plog_info("audio pid = %d\n",AudioPara.pid);
	plog_info("audio codec = %d\n",media->audios[media->audio_index].codec);
	m_player->InitAudio(&AudioPara);

	m_player->StartPlay();
	m_player->VideoShow();
	m_player->playerback_register_evt_cb(MediaProcessorEvent, avsobj);
	avsobj->started = 1;
	avsobj->status = AVS_STATUS_PLAY;

	plog_error("Func:%s Out\n", __FUNCTION__);
	avsobj->p_media = media;
	return 0;
}

/**
 * @brief 				ֹͣavstage
 * @param avsobj 		avstage ������ 
 * @param hold_lastpic  �Ƿ������һ֡
 * @param startmode	 	����ģʽ
 * @return 				0 �ɹ� ���� �����
 */
int sw_avstage_stop(swavstage_t* avsobj, bool hold_lastpic)
{
	m_player->SetStopMode(hold_lastpic);
	m_player->Stop();
	avsobj->started = 0;
	avsobj->status = AVS_STATUS_DEC_STOP;
	return 0;
}


/**
 * @brief 				��黺������Ƿ�������
 * @param avsobj 		avstage ������ 
 * @return true ������, ���� �������л������
 */
bool sw_avstage_is_over(swavstage_t* avsobj)
{
	long depth = 0, size = 0;
	long pts = -1;

	if(m_player == NULL)
		return true;
		
	if(m_player->GetBufferStatus((long *)&size, (long *)&depth) == 0)
	{
		if(depth >= 0 && depth > 1024 * 800)
		{
			avsobj->pts_same_count = 0;
			return false;
		}
	}

	pts = m_player->GetCurrentPts();
	if(pts == avsobj->last_pts)
	{
		if(avsobj->pts_same_count < 5)
		{
			avsobj->pts_same_count++;
			return false;
		}
		else
		{
			avsobj->last_pts = -1;
			return true;
		}
	}
	else
	{
		avsobj->last_pts = pts;
		avsobj->pts_same_count = 0;
		return false;
	}

	avsobj->pts_same_count = 0;
	return true;
}

/**
 * @brief 				��AVstage �л�ȡ���л���
 * @param avsobj 		avstage ������ 
 * @param type_id 		�������[ϵͳ��/��Ƶ/��Ƶ]
 * @param buf			����bufָ��
 * @param size			[in]��Ҫ�Ĵ�С [out]ʵ�ʷ��صĴ�С
 * @return 				0 �ɹ�, -2 ������type_idָ�������, ���� �����
 */
int sw_avstage_get_buffer(swavstage_t* avsobj, int type_id, uint8_t** buf, int* size)
{
	m_player->GetWriteBuffer((IPTV_PLAYER_STREAMTYPE_e)type_id, buf, (unsigned int *)size);
	return 0;
}

/**
 * @brief 				�ύ���õ���ݵ�AVStage
 * @param avsobj 		avstage ������ 
 * @param type_id 		�������[ϵͳ��/��Ƶ/��Ƶ]
 * @param timestamp 	type_idΪES��ʱΪʱ���[45K]
 * @param buf			�ύ��bufָ��
 * @param size			�ύ��buf��С
 * @param skip			�����Ч��ݵĴ�С
 * @return 				0 �ɹ� ���� �����
 */
int sw_avstage_put_buffer(swavstage_t* avsobj, int type_id, int64_t timestamp, uint8_t* buf, int size, int skip)
{
	int ret = -1;

	while(-1 == ret)
		ret = m_player->WriteData((IPTV_PLAYER_STREAMTYPE_e)type_id, buf, size, timestamp);
	
	return 0;
}

/**
 * @brief 				����״̬
 * @param avsobj 		avstage ������ 
 * @param status_id 	״̬����
 * @param param 		״̬����
 * @param param 		״̬��չ����
 * @return 				0 �ɹ� ���� �����
 */
int sw_avstage_set_status(swavstage_t* avsobj, int status_id, unsigned long param, unsigned long ext_param)
{
	plog_info("func[%s], status_id = %d\n", __FUNCTION__, status_id);
	if (status_id == AVS_STATUS_PLAY)
	{
		int scale = (int)param;
		plog_info("PLAY scale = %d",scale);

		if(scale != 1000)
			m_player->Fast();
		else
		{
			if(avsobj->status == AVS_STATUS_PAUSED)
				m_player->Resume();
			else
				m_player->StopFast();
		}
	}
	else if (status_id == AVS_STATUS_PAUSED)
	{
		m_player->Pause();
	}
	else if (status_id == AVS_STATUS_SEEK)
	{
		m_player->Seek();
	}
	else if (status_id == AVS_STATUS_DEC_START)
	{
		m_player->StartPlay();
	}
	else if (status_id == AVS_STATUS_DEC_STOP)
	{
		m_player->Stop();
	}
	avsobj->status = status_id;
	return 0;
}

/**
 * @brief 				��ȡ����
 * @param avsobj 		avstage ������ 
 * @param property_id 	��������
 * @param value 		������Ե��ڴ�
 * @param size	 		������Ե��ڴ��С
 * @return 				0 �ɹ� ���� �����
 */
int sw_avstage_get_property(swavstage_t* avsobj, swavs_property_e property_id, void *value, int size)
{
	if(property_id == AVS_PROPERTY_AUDIO_CHANNEL)
	{
		*((int*)value) = m_player->GetAudioBalance();
	}
	else if(property_id == AVS_PROPERTY_FIFOINFO)
	{
		swfifoinfo_t *fifoinfo = (swfifoinfo_t *)value;
		m_player->GetBufferStatus((long *)&(fifoinfo->size), (long *)&(fifoinfo->depth));
	}
	else if(property_id == AVS_PROPERTY_VIDEOINFO)
	{
		int width = 0, height = 0;
		videoinfo_t *videoinfo = (videoinfo_t *)value;
		memset(videoinfo, 0, sizeof(videoinfo_t));
		
		m_player->GetVideoPixels(&width, &height);
		videoinfo->width = width;
		videoinfo->height = width;
	}
	else if(property_id == AVS_PROPERTY_PLAYTIME)
	{
		*((long *)value) = m_player->GetCurrentPts() * 45;
	}
	else if(property_id == AVS_PROPERTY_VIDEO_MODE)
	{
		*((swpigmode_e*)value) = avsobj->mode;
	}
	else if(property_id == AVS_PROPERTY_VIDEO_RECT)
	{
		swrect_t *p_rect  = (swrect_t *)value;
		if(avsobj->mode == SW_PIGMODE_FULL)
		{	
			p_rect->x = 0;
			p_rect->y = 0;
			p_rect->width = 1280;
			p_rect->height = 720;
		}
		else
			memcpy(p_rect, &(avsobj->rect), sizeof(swrect_t));
	}
	return 0;
}


/**
 * @brief 				��������
 * @param avsobj 		avstage ������ 
 * @param property_id 	��������
 * @param value 		����ֵ�ڴ�
 * @param size	 		����ֵ�ڴ��С
 * @return 				0 �ɹ� ���� �����
 */
int sw_avstage_set_property(swavstage_t* avsobj, swavs_property_e property_id, void *value, int size)
{
	int x1 = 0,y1 = 0, w1 = 0,h1 = 0,w = 0,h = 0;

	if(property_id == AVS_PROPERTY_VIDEO_TARGET)
	{
		m_player->SetSurfaceTexture((ISurfaceTexture *) value);
	}
	else if(property_id == AVS_PROPERTY_VIDEO_MODE)
	{
		avsobj->mode = *(( swpigmode_e*)value);
		m_player->GetVideoPixels(&w, &h);
		w = (w <= 0) ? 1280 : w;
		h = (h <= 0) ? 720 : h;
		if(avsobj->mode == SW_PIGMODE_FULL)
			m_player->SetVideoWindow(0,0,w,h);
		else
			m_player->SetVideoWindow(avsobj->rect.x*w/1280, avsobj->rect.y*h/720, avsobj->rect.width*w/1280, avsobj->rect.height*h/720);
	}
	else if (property_id == AVS_PROPERTY_VIDEO_RECT)
	{
		swrect_t *p_rect = (swrect_t *)value;
		avsobj->rect.x = p_rect->x;
		avsobj->rect.y = p_rect->y;
		avsobj->rect.width = p_rect->width;
		avsobj->rect.height = p_rect->height;
		m_player->SetVideoWindow(avsobj->rect.x, avsobj->rect.y, avsobj->rect.width, avsobj->rect.height);
	}
	else if (property_id == AVS_PROPERTY_VIDEO_VISIBLE)
	{
		bool is_visible = *((bool*)value);
		if (is_visible)
			m_player->VideoShow();
		else
			m_player->VideoHide();
	}
	else if((int)property_id == AVS_PROPERTY_VIDEO_CONTENTMODE)
	{
		swcontentmode_e contentmode = *((swcontentmode_e *)value);
		if(SW_CONTENTMODE_LETTERBOX == contentmode)
			m_player->SetContentMode(IPTV_PLAYER_CONTENTMODE_LETTERBOX);
		else if(SW_CONTENTMODE_FULL == contentmode)
			m_player->SetContentMode(IPTV_PLAYER_CONTENTMODE_FULL);
		
	}
	else if (property_id == AVS_PROPERTY_AUDIO_CHANNEL)
	{
		int audio_channel = *((int*)value);
		m_player->SetAudioBalance(audio_channel);
	}
	else if (property_id == AVS_PROPERTY_APID)
	{
		int audio_track_pid = *((int*)value);
		AUDIO_PARA_T AudioPara;
		if(avsobj->p_media->system_num != 0)
			m_player->SwitchAudioTrack(audio_track_pid, &AudioPara);
		else
		{
			int i = 0;
			for(i = 0; i < avsobj->p_media->audio_num; i++)
			{
				if(avsobj->p_media->audios[i].pid == audio_track_pid)
				{
					AudioPara.pid = avsobj->p_media->audios[i].pid;
					AudioPara.nSampleRate = avsobj->p_media->audios[i].samplerate;
					AudioPara.nChannels = avsobj->p_media->audios[i].channels;
					AudioPara.block_align = avsobj->p_media->audios[i].block_align;
					AudioPara.bit_per_sample = swformat2bitpersample(avsobj->p_media->audios[i].sample_format);
					AudioPara.nExtraSize = avsobj->p_media->audios[i].spec_len;
					if(AudioPara.nExtraSize)
					{
						AudioPara.pExtraData = (unsigned char *)malloc(AudioPara.nExtraSize * sizeof(unsigned char));
						if(AudioPara.pExtraData)
							memcpy(AudioPara.pExtraData, avsobj->p_media->audios[i].spec_data, AudioPara.nExtraSize);
					}
					AudioPara.aFmt = (aformat_t)swcodec2aformat(avsobj->p_media->audios[i].codec);
					m_player->SwitchAudioTrack(audio_track_pid, &AudioPara);
				}
			}
		}
	}
	else if ((int)property_id == (int)PLAYER_PROPERTY_SUB_TRACK)
	{
		int subtitle_pid = *((int*)value);
	}
	else if ((int)property_id == (int)PLAYER_PROPERTY_SUB_SHOW)
	{

	}
	return 0;
}


/**
 * @brief 				�����¼��ص����?��
 * @param avsobj 		avstage ������ 
 * @param on_event 		�ص�����ָ��
 * @param handle 		�û����
 * @return 				��
 */
void sw_avstage_set_event_callback(swavstage_t* avsobj, avs_event_callback on_event, void *handle)
{
	avsobj->event_handle = (void *)handle;
	avsobj->p_event_callback = on_event;
	
	return ;
}

