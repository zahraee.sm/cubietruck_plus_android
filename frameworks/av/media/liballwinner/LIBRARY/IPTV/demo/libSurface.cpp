#include <stdio.h>
#include <unistd.h>
#include <utils/Log.h>
#include <binder/IServiceManager.h>
#include <binder/IPCThreadState.h>
#include <gui/ISurfaceComposer.h>
#include <gui/Surface.h>
#include <gui/SurfaceComposerClient.h>
#include <ui/DisplayInfo.h>
#include <utils/RefBase.h>
#include <utils/Log.h>
#include <cutils/memory.h>
#include <android/native_window.h>
using namespace android;
#include "libSurface.h"

#define LOG_TAG "libSurface"
#define PLANE_WIDTH 1280
#define PLANE_HEIGHT 720

static sp<Surface> surface = NULL;
static sp<SurfaceControl> sfcontrol = NULL;
static sp<Surface> videoSurface = NULL;
static sp<SurfaceControl> videoSfControl = NULL;
static sp<SurfaceComposerClient> client = NULL;
static DisplayInfo dinfo;

int sw_surface_init()
{
	sp<ProcessState> proc(ProcessState::self());
	ProcessState::self()->startThreadPool();

	client = new SurfaceComposerClient();
	if (client != NULL)
	{
	    sp<IBinder> display(SurfaceComposerClient::getBuiltInDisplay(
            ISurfaceComposer::eDisplayIdMain));
		status_t status = client->getDisplayInfo(display, &dinfo);
		ALOGD("getDisplayInfo,status(%d)", status);
		if (status != 0)
		{
			dinfo.w = PLANE_WIDTH;
			dinfo.h = PLANE_HEIGHT;
		}

	}
	ALOGD("dinfo.w(%d), dinfo.h(%d)", dinfo.w, dinfo.h);
	#if 0
	sfcontrol = client->createSurface(String8("SurfaceView"), dinfo.w, dinfo.h, PIXEL_FORMAT_RGBA_8888,0);
	if(sfcontrol == NULL)
		return -1;
	SurfaceComposerClient::openGlobalTransaction();
	sfcontrol->setSize(dinfo.w, dinfo.h);
	sfcontrol->setPosition(1, 1);
	sfcontrol->setLayer(0x2000000);
	sfcontrol->show();
	SurfaceComposerClient::closeGlobalTransaction();
	surface = sfcontrol->getSurface();

	videoSfControl = client->createSurface(String8("SurfaceView"), dinfo.w, dinfo.h, PIXEL_FORMAT_RGBX_8888,0);
	if(videoSfControl == NULL)
		return -1;

	SurfaceComposerClient::openGlobalTransaction();
	videoSfControl->setSize(dinfo.w, dinfo.h);
	videoSfControl->setPosition(1, 1);
	videoSfControl->setLayer(0x2000000 - 1);
	videoSfControl->show();
	SurfaceComposerClient::closeGlobalTransaction();
	videoSurface = videoSfControl->getSurface();
	ALOGD("videoSurface.get()%p", videoSurface.get());
	#else
	videoSfControl = client->createSurface(
                String8("Test Surface"), dinfo.w, dinfo.h, HAL_PIXEL_FORMAT_YCrCb_420_SP, 0);
	if(videoSfControl == NULL)
		return -1;

	SurfaceComposerClient::openGlobalTransaction();
	videoSfControl->setLayer(0x7fffffff);
	videoSfControl->show();

	videoSfControl->setSize(dinfo.w, dinfo.h);
	videoSfControl->setPosition(1, 1);
	//videoSfControl->setLayer(0x2000000 - 1);
	//videoSfControl->show();
	SurfaceComposerClient::closeGlobalTransaction();
	videoSurface = videoSfControl->getSurface();
    ALOGD("%s surface0 = %p", __func__, videoSurface.get());
    ANativeWindow_Buffer outBuffer;
    videoSurface->lock(&outBuffer, NULL);
    memset((char*)outBuffer.bits, 0x10,(outBuffer.height * outBuffer.stride));
    memset((char*)outBuffer.bits + outBuffer.height * outBuffer.stride,0x80,(outBuffer.height * outBuffer.stride)/2);
    videoSurface->unlockAndPost();
	#endif

    //ANativeWindow *anw = (ANativeWindow*) (videoSurface.get());
    /*
    void* tmp = videoSurface.get();
    Surface* tmp1 = (Surface *)tmp;
    void* tmp2 = tmp1;
    */
/*
    Surface* tmp = (Surface*)videoSurface.get();
	ALOGD("*****tmp(%p)", tmp);
	
    //unsigned int tmp3 = (unsigned int)tmp;
    void* tmp4 = (void *)(tmp);
	ALOGD("*****tm4(%p)", tmp4);
    Surface* tmp5 = (Surface*)(tmp4);
	ALOGD("*****tm5(%p)", tmp5);
    ANativeWindow* anw = (ANativeWindow*)(tmp5);


    //ALOGD("%s videoSurface.get()=%p, tmp=%p, tmp1=%p, tmp2=%p, tmp3=%x, tmp4=%p, anw=%p",
        //__func__, videoSurface.get(), tmp, tmp1, tmp2, tmp3, tmp4, anw);
    int err = anw->perform(anw,NATIVE_WINDOW_GETPARAMETER,
        NATIVE_WINDOW_CMD_GET_SURFACE_TEXTURE_TYPE,0);


    ALOGD("%s @@@err=%d", __func__, err);
*/
	return 0;
}

void sw_surface_exit()
{
	client = NULL;
	surface = NULL;
	videoSurface = NULL;
	sfcontrol = NULL;
	videoSfControl = NULL;

	return ;
}

void* swgetSurface( )
{
	return surface.get();
}

void* swget_VideoSurface( )
{
    ALOGD("%s surface = %p", __func__, videoSurface.get());
	return videoSurface.get();
	//return (videoSurface->getSurfaceTexture()).get();
}
