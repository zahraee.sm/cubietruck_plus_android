
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <errno.h>

#include "BootHead.h"
#include "Utils.h"

#define NAND_BLKBURNBOOT0 		_IO('v',127)
#define NAND_BLKBURNUBOOT 		_IO('v',128)

#define SECTOR_SIZE	512
/*Normal boot 0 and boot 1 */
#define SD_BOOT0_SECTOR_START	16
#define SD_BOOT0_SIZE_KBYTES	32

#define SD_UBOOT_SECTOR_START   38192
#define SD_UBOOT_SIZE_KBYTES	640

/*TOC0 : sector 16 - 256*/
#define SD_TOC0_SECTOR_START   16
#define SD_TOC0_SIZE_KBYTES	   120

/*TOC1 : sector 32800 - 40960 */
#define SD_TOC1_SECTOR_START   32800
#define SD_TOC1_SIZE_KBYTES	   4080 
static int sd_boot0_start , sd_boot0_len;
static int sd_boot1_start , sd_boot1_len;

/*some chip (e.g. H8) BOOT0 save in mmcblk0boot0 */
#define SD_BOOT0_PERMISION "/sys/block/mmcblk0boot0/force_ro"
#define DEVNODE_PATH_SD_BOOT0 "/dev/block/mmcblk0boot0"

void SdBootInit(void)
{
	static int inited = 0;

	if(inited == 1)
		return ;
    
	if(check_soc_is_secure()){ //secure
		sd_boot0_start	=	SD_TOC0_SECTOR_START *  SECTOR_SIZE;
		sd_boot0_len	=	SD_TOC0_SIZE_KBYTES * 1024;
		sd_boot1_start	=	SD_TOC1_SECTOR_START * SECTOR_SIZE ;
		sd_boot1_len	=	SD_TOC1_SIZE_KBYTES * 1024;
	}else{
		sd_boot0_start	=	SD_BOOT0_SECTOR_START *  SECTOR_SIZE;
		sd_boot0_len	=	SD_BOOT0_SIZE_KBYTES * 1024;
		sd_boot1_start	=	SD_UBOOT_SECTOR_START * SECTOR_SIZE ;
		sd_boot1_len	=	SD_UBOOT_SIZE_KBYTES * 1024;
	}
	inited =1 ;
	return ;
}

static int writeSdBoot(int fd, void *buf, off_t offset, size_t bootsize){
	if (lseek(fd, 0, SEEK_SET) == -1) {
		bb_debug("reset the cursor failed! the error num is %d:%s\n",errno,strerror(errno));
		return -1;
	}

	if (lseek(fd, offset, SEEK_CUR) == -1) {
		bb_debug("lseek failed! the error num is %d:%s\n",errno,strerror(errno));
		return -1;
	}
	bb_debug("Write sd boot : offset = 0x%x, len= 0x%x\n", offset, bootsize);
	int result = write(fd, buf, bootsize);
	fsync(fd);
	return result;
}

static int readSdBoot(int fd ,off_t offset, size_t bootsize, void *buffer){
	memset(buffer, 0, bootsize);
	if (lseek(fd, 0, SEEK_SET) == -1) {
		bb_debug("reset the cursor failed! the error num is %d:%s\n",errno,strerror(errno));
		return -1;
	}

	if (lseek(fd, offset, SEEK_CUR) == -1) {
		bb_debug("lseek failed! the error num is %d:%s\n",errno,strerror(errno));
		return -1;
	}

	return	read(fd,buffer,bootsize);
}

static int openDevNode(const char *path){
	int fd = open(path, O_RDWR);
	if (fd == -1){
		bb_debug("open device node failed ! errno is %d : %s\n", errno, strerror(errno));
	}
	return fd;
}

size_t readSdBoot0(char *path, void **buffer){

	*buffer = malloc(sd_boot0_len);
	int fd = openDevNode(path);
	if (fd == -1)
		return -1;
	
	return readSdBoot(fd, sd_boot0_start, sd_boot0_len, *buffer);

}

//just for boot2.0
size_t readSdUboot(char *path, void **buffer){
	bb_debug("reading SD Uboot!\n");
	
	int fd = openDevNode(path);
	if (fd == -1) 
		return -1;
	uboot_file_head *ubootHead;
	ubootHead = malloc(sizeof(uboot_file_head));
	readSdBoot(fd, sd_boot1_start, sizeof(uboot_file_head), ubootHead);
	size_t length = (size_t)ubootHead->length;
	free(ubootHead);

	*buffer = malloc(length);
	bb_debug("the inner uboot length is %d\n", length);
	return readSdBoot(fd, sd_boot1_start, length, *buffer);
}

int burnSdBoot0(BufferExtractCookie *cookie, char *path)
{
	if (checkBoot0Sum(cookie)){
		bb_debug("illegal binary file!\n");
		return -1;
	}
    int ret = 0;
    int fd = -1;

    /*Try to write boot0 on DEVNODE_PATH_SD_BOOT0*/
    fd = openDevNode(DEVNODE_PATH_SD_BOOT0);
    if (fd > 0)
    {
        int pmsFd = open(SD_BOOT0_PERMISION,O_WRONLY);
        if (pmsFd > 0)
        {
            ret = write(pmsFd,"0",1);
            close(pmsFd);
        }
        else
        {
	        bb_debug("can't open %s :%s \n", SD_BOOT0_PERMISION,strerror(errno));
            close(fd);
            return -1;
        }
        if (ret < 0)
        {
	        bb_debug("can't write 0 to %s :%s \n", SD_BOOT0_PERMISION,strerror(errno));
            close(fd);
            return ret;
        }

	    bb_debug("burnSdBoot0 in mmcblk0boot0 %d\n", ret);
        ret = writeSdBoot(fd, cookie->buffer, 0, sd_boot0_len);
        if (ret > 0){
		    bb_debug("burnSdBoot0 succeed! on %d writed %d bytes\n", 0, ret);
	    }
        fsync(fd);

	    close(fd);
    }
    fd = openDevNode(path);
	if (fd == -1){
       return -1;
	}

    bb_debug("burnSdBoot0 in mmcblk0:offset = 0x%x, len =0x%x\n", sd_boot0_start, cookie->len);
	ret = writeSdBoot(fd, cookie->buffer, sd_boot0_start , cookie->len);
	if (ret > 0){
	    bb_debug("burnSdBoot0 succeed! on %d writed %d bytes\n",sd_boot0_start, ret);
    }
    fsync(fd);
    close(fd);

	if (ret > 0){
		bb_debug("burnSdBoot0 succeed! writed %d bytes\n", ret);
	}
	return ret;
}

int burnSdUboot(BufferExtractCookie *cookie, char *path){
	if (checkUbootSum(cookie)){
		bb_debug("illegal uboot binary file!\n");
		return -1;
	}

	bb_debug("uboot binary length is %ld\n",cookie->len);
	int fd = openDevNode(path);
	if (fd == -1){
		return -1;
	}
	int ret = -1;
	int defaultOffset = -1;
	
	if(!check_soc_is_secure()){
		/*no secure case*/
		defaultOffset = getUbootstartsector(cookie);

		if (defaultOffset>0)
		{
			ret = writeSdBoot(fd, cookie->buffer, defaultOffset*SECTOR_SIZE, cookie->len);
			if (ret > 0){
				bb_debug("burnSdUboot succeed! on %d writed %d bytes\n",defaultOffset*SECTOR_SIZE, ret);
			}
		}
	}
	ret = writeSdBoot(fd, cookie->buffer, sd_boot1_start, cookie->len);
	if (ret > 0){
		bb_debug("burnSdUboot succeed! on %d writed %d bytes\n",sd_boot1_start, ret);
	}

	fsync(fd);
	close(fd);

	return ret;
}

int burnNandBoot0(BufferExtractCookie *cookie, char *path){

	if (checkBoot0Sum(cookie)){
		bb_debug("illegal boot0 binary file!\n");
		return -1;
	}

	int fd = openDevNode(path);
	if (fd == -1){
		return -1;
	}

	clearPageCache();

	int ret = ioctl(fd,NAND_BLKBURNBOOT0,(unsigned long)cookie);

	if (ret) {
		bb_debug("burnNandBoot0 failed ! errno is %d : %s\n", errno, strerror(errno));
	}else{
		bb_debug("burnNandBoot0 succeed!\n");
	}

	close(fd);
	return ret;
}

int burnNandUboot(BufferExtractCookie *cookie, char *path){
	if (checkUbootSum(cookie)){
		bb_debug("illegal uboot binary file!\n");
		return -1;
	}

	int fd = openDevNode(path);
	if (fd == -1){
		return -1;
	}

	clearPageCache();

	int ret = ioctl(fd,NAND_BLKBURNUBOOT,(unsigned long)cookie);
	if (ret) {
		bb_debug("burnNandUboot failed ! errno is %d : %s\n", errno, strerror(errno));
	}else{
		bb_debug("burnNandUboot succeed!\n");
	}
	close(fd);

	return ret;
}
